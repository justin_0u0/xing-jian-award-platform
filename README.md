# xing-jian-award-platform

Nthu DSA Xing Jian Award Frontend Platform

# How to Install
- Make sure Node is installed
- `git checkout dev` branch & `git pull`
- Run `npm install`
- Run `npm run dev`
- Access `localhost:3000` in the browser